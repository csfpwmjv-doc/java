const React = require('react');

class Index extends React.Component {

    render() {
        const SplashContainer = props => (
            <div className="homeContainer">
                <div className="homeSplashFade">
                    <div className="wrapper homeWrapper">{props.children}</div>
                </div>
            </div>
        );

        const Logo = () => (
            <img src={`${this.props.config.baseUrl}${this.props.config.homeIcon}`} alt={this.props.config.title} height={256}/>
        );

        const ProjectTitle = () => (
            <div className="inner">
                <h2 className="projectTitle">
                    {this.props.config.title}
                    <small>{this.props.config.tagline}</small>
                </h2>
            </div>
        );

        return (
            <SplashContainer>
                <Logo/>
                <ProjectTitle/>
            </SplashContainer>
        );
    }
}

module.exports = Index;